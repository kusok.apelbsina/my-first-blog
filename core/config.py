from os import environ
from databases import Database


DATABASE_URL = environ.get('EE_DATABASE_URL', '')

database = Database(DATABASE_URL)
