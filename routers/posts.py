from models.posts import PostDetailModel, PostModel
from models.users import User
from utils import posts as posts_utils
from utils.dependecies import get_current_user
from fastapi import APIRouter, Depends, HTTPException, status, Response
from http import HTTPStatus


router = APIRouter()


@router.post('/posts', response_model=PostDetailModel, status_code=201)
async def create_post(
        post: PostModel, current_user: User = Depends(get_current_user)):
    post = await posts_utils.create_post(post, current_user)
    return post


@router.get('/posts')
async def get_posts(page: int = 1):
    total_count = await posts_utils.get_posts_count()
    posts = await posts_utils.get_posts(page)
    return {'total_count': total_count, 'results': posts}


@router.get('/posts/{post_id}', response_model=PostDetailModel)
async def get_post(post_id: int):
    return await posts_utils.get_post(post_id)


@router.put('/posts/{post_id}', response_model=PostDetailModel)
async def update_post(
        post_id: int,
        post_data: PostModel, current_user: User = Depends(get_current_user)
):
    post = await posts_utils.get_post(post_id)
    if post['user_id'] != current_user['id']:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You don't have access to modify this post",
        )
    await posts_utils.update_post(post_id=post_id, post=post_data)
    return await posts_utils.get_post(post_id)


@router.delete('/posts/{post_id}', status_code=204)
async def delete_post(
        post_id: int, current_user: User = Depends(get_current_user)):
    post = await posts_utils.get_post(post_id)
    if not post:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="This post_id isn't right",
        )
    if post['user_id'] != current_user['id']:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You don't have access to modify this post",
        )
    await posts_utils.delete_post(post_id=post_id)
    return Response(status_code=HTTPStatus.NO_CONTENT.value)
