from fastapi import APIRouter, Depends, HTTPException, Response
from fastapi.security import OAuth2PasswordRequestForm
from models import users
from utils import users as users_utils
from http import HTTPStatus

from utils.dependecies import get_current_user

router = APIRouter()


@router.get("/users/me", response_model=users.UserBase)
async def read_users_me(current_user: users.User = Depends(get_current_user)):
    return current_user


@router.delete("/users/me", status_code=204)
async def delete_user(current_user: users.User = Depends(get_current_user)):
    await users_utils.delete_user(user_id=current_user['id'])
    return Response(status_code=HTTPStatus.NO_CONTENT.value)


@router.post("/sigh-up", response_model=users.User)
async def create_user(user: users.UserCreate):
    db_user = await users_utils.get_user_by_email(email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registed")
    return await users_utils.create_user(user=user)


@router.post('/auth', response_model=users.TokenBase)
async def auth(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await users_utils.get_user_by_email(email=form_data.username)
    if not user:
        raise HTTPException(
            status_code=400, detail='Incorrect email or password')
    if not users_utils.validate_password(
        password=form_data.password, hashed_password=user['hashed_password']
    ):
        raise HTTPException(
            status_code=400, detail='Incorrect email or password')
    return await users_utils.create_user_token(user_id=user['id'])
