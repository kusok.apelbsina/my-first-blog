from models.comments import CommentDetailModel, CommentUpdateModel
from models.comments import CommentModel
from models.users import User
from utils import comments as comments_utils
from utils.dependecies import get_current_user
from fastapi import APIRouter, Depends, HTTPException, status, Response
from http import HTTPStatus


router = APIRouter()


@router.post('/comments', response_model=CommentDetailModel, status_code=201)
async def create_comment(
        comment: CommentModel, current_user: User = Depends(get_current_user)
):
    post = await comments_utils.create_comment(comment, current_user)
    return post


@router.get('/posts/{post_id}/comments')
async def get_comments_by_post(post_id: int, page: int = 1):
    comments = await comments_utils.get_comments(post_id, page)
    return {'results': comments}


@router.get('/comments/{comment_id}', response_model=CommentDetailModel)
async def get_comment(comment_id: int):
    return await comments_utils.get_comment(comment_id)


@router.put('/comments/{comment_id}', response_model=CommentDetailModel)
async def update_comment(
    comment_id: int,
    comment_data: CommentUpdateModel,
    current_user: User = Depends(get_current_user)
):
    comment = await comments_utils.get_comment(comment_id)
    if comment['user_id'] != current_user['id']:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You don't have access to modify this comment",
        )
    await comments_utils.update_comment(
        comment_id=comment_id, comment=comment_data
    )
    return await comments_utils.get_comment(comment_id)


@router.delete('/comments/{comment_id}', status_code=204)
async def gelete_comment(
        comment_id: int, current_user: User = Depends(get_current_user)
):
    comment = await comments_utils.get_comment(comment_id)
    if not comment:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="This comment's id isn't right",
        )
    if comment['user_id'] != current_user['id']:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You don't have access to modify this post",
        )
    await comments_utils.delete_comment(comment_id=comment_id)
    return Response(status_code=HTTPStatus.NO_CONTENT.value)
