from pydantic import BaseModel


class CommentModel(BaseModel):
    text: str
    post_id: int


class CommentDetailModel(BaseModel):
    user_id: int
    post_id: int
    text: str
    id: int


class CommentUpdateModel(BaseModel):
    text: str
