from pydantic import BaseModel
from datetime import datetime


class PostModel(BaseModel):
    title: str
    content: str
    created_at: datetime


class PostDetailModel(BaseModel):
    id: int
    title: str
    content: str
    created_at: datetime
    user_id: int
