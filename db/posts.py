import sqlalchemy
from .users import users_table

metadata = sqlalchemy.MetaData()

posts_table = sqlalchemy.Table(
    'posts',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column('title', sqlalchemy.String(200)),
    sqlalchemy.Column('content', sqlalchemy.String(1000)),
    sqlalchemy.Column(
        'user_id',
        sqlalchemy.ForeignKey(users_table.c.id, ondelete='CASCADE'),
    ),
    sqlalchemy.Column('created_at', sqlalchemy.DateTime()),
)
