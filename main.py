from fastapi import FastAPI
from db.base import database
import uvicorn
from routers import users, posts, comments

app = FastAPI()
app.include_router(users.router)
app.include_router(posts.router)
app.include_router(comments.router)


@app.get("/")
async def read_root():
    return {'name': 'admin'}


@app.on_event('startup')
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


if __name__ == "__main__":
    uvicorn.run('main:app', port=8080, host='0.0.0.0', reload=True)
