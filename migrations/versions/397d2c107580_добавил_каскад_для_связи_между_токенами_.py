"""Добавил каскад для связи между токенами к юзерам

Revision ID: 397d2c107580
Revises: 5b089aac340e
Create Date: 2021-11-04 04:40:53.143395

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '397d2c107580'
down_revision = '5b089aac340e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('tokens_user_id_fkey', 'tokens', type_='foreignkey')
    op.create_foreign_key(None, 'tokens', 'users', ['user_id'], ['id'], ondelete='CASCADE')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'tokens', type_='foreignkey')
    op.create_foreign_key('tokens_user_id_fkey', 'tokens', 'users', ['user_id'], ['id'])
    # ### end Alembic commands ###
