"""empty message

Revision ID: c99604e89721
Revises: 3c2077eacd90
Create Date: 2021-11-04 02:22:10.833879

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c99604e89721'
down_revision = '3c2077eacd90'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
