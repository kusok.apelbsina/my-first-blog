from datetime import datetime

from db.base import database
from db.posts import posts_table
from db.users import users_table
from models import posts as post_shema
from sqlalchemy import func, select


async def create_post(post: post_shema.PostModel, user):
    query = (
        posts_table.insert()
        .values(
            title=post.title,
            content=post.content,
            created_at=datetime.now(),
            user_id=user['id'],
        )
        .returning(
            posts_table.c.id,
            posts_table.c.title,
            posts_table.c.content,
            posts_table.c.user_id,
            posts_table.c.created_at,
        )
    )
    post = await database.fetch_one(query)
    post = dict(zip(post, post.values()))
    post['user_name'] = user['name']
    return post


async def get_post(post_id: int):
    query = (
        select(
            [
                posts_table.c.id,
                posts_table.c.title,
                posts_table.c.content,
                posts_table.c.user_id,
                posts_table.c.created_at,
                users_table.c.name.label('user_name'),
            ]
        )
        .select_from(posts_table.join(users_table))
        .where(posts_table.c.id == post_id)
    )
    return await database.fetch_one(query)


async def get_posts(page: int):
    max_per_page = 10
    offset = (page - 1) * max_per_page
    query = (
        select(
            [
                posts_table.c.id,
                posts_table.c.title,
                posts_table.c.content,
                posts_table.c.user_id,
                posts_table.c.created_at,
                users_table.c.name.label("user_name"),
            ]
        )
        .select_from(posts_table.join(users_table))
        .limit(max_per_page)
        .offset(offset)
    )
    return await database.fetch_all(query)


async def get_posts_count():
    query = select([func.count()]).select_from(posts_table)
    return await database.fetch_all(query)


async def update_post(post_id: int, post: post_shema.PostModel):
    query = (
        posts_table.update()
        .where(posts_table.c.id == post_id)
        .values(title=post.title, content=post.content)
    )
    return await database.execute(query)


async def delete_post(post_id: int):
    query = (
        posts_table.delete()
        .where(posts_table.c.id == post_id)
    )
    return await database.execute(query)
