from utils import users as users_utils
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer


oauth2_sheme = OAuth2PasswordBearer(tokenUrl='/auth')


async def get_current_user(token: str = Depends(oauth2_sheme)):
    user = await users_utils.get_user_by_token(token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication crendentaild",
            headers={'WWW-Authenticate': 'Bearer'}
        )
    if not user['is_active']:
        return HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='Inactive user'
        )
    return user
