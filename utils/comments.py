from db.base import database
from db.comments import comments_table
from db.users import users_table
from models import comments as comment_shema
from sqlalchemy import select


async def create_comment(
        comment: comment_shema.CommentModel, user):
    query = (
        comments_table.insert()
        .values(
            text=comment.text,
            user_id=user['id'],
            post_id=comment.post_id,
        )
        .returning(
            comments_table.c.id,
            comments_table.c.text,
            comments_table.c.user_id,
            comments_table.c.post_id,
        )
    )
    comment = await database.fetch_one(query)
    comment = dict(zip(comment, comment.values()))
    return comment


async def get_comment(comment_id: int):
    query = (
        select([
            comments_table.c.id,
            comments_table.c.user_id,
            comments_table.c.post_id,
            comments_table.c.text,
            users_table.c.name.label('user_name'),
        ])
        .select_from(comments_table.join(users_table))
        .where(comments_table.c.id == comment_id)
    )
    return await database.fetch_one(query)


async def get_comments(post_id: int, page: int):
    max_per_page = 10
    offset = (page - 1) * max_per_page
    query = (
        select([
            comments_table.c.id,
            comments_table.c.user_id,
            comments_table.c.text,
            users_table.c.name.label('user_name'),
        ])
        .where(comments_table.c.post_id == post_id)
        .limit(max_per_page)
        .offset(offset)
    )
    return await database.fetch_all(query)


async def update_comment(comment_id: int, comment: comment_shema.CommentUpdateModel):
    query = (
        comments_table.update()
        .where(comments_table.c.id == comment_id)
        .values(text=comment.text)
    )
    return await database.execute(query)


async def delete_comment(comment_id: int):
    query = (
        comments_table.delete()
        .where(comments_table.c.id == comment_id)
    )
    return await database.execute(query)
